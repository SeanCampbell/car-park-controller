library ieee;
use ieee.std_logic_1164.all;

entity entry_lane_tb is
end entity entry_lane_tb;

architecture v1 of entry_lane_tb is
	--Component being tested
	component entry_entity is
		port(
			clk         : in  std_logic; --clock 
			button_pressed		: in std_logic; --INput from button to retrieve ticket
			emergency			: in std_logic; --Input from higher level to inform system of emergency state, '0' indicates no emergency
			carpark_status		: in std_logic; --Input from higher level to inform system if car park is full, '0' indicates that it is not full
			front_barrier, under_barrier, after_barrier  : in  std_logic; -- the barrier sensors, we will have to make these visible to upper system I think
			
			barrier_position     : out std_logic; --output to the barrier to get it to raise or lower		
			print_ticket		: out std_logic; --Output to ticket printer
			car_passed			: out std_logic --Output to higher level which informs it that a car has entered
		);
	end component;
	--------------------------------------
    -- Constants used within the design
    --------------------------------------
    constant T_clk      : time    := 10 ns;	-- define clock period
	constant T_CHANGE : time := 11 ns;
	constant ERR0 : string := "Predicted State 0 output violation";
	constant ERR1 : string := "Predicted State 1 output violation";
	constant ERR2 : string := "Predicted State 2 output violation";
	constant ERR3 : string := "Predicted State 3 output violation";
	constant ERR4 : string := "Predicted State 4 output violation";
	------------------------------------------------------------------------------
    -- Stimulus signals - signals mapped to the input ports of the tested entity
    ------------------------------------------------------------------------------
    signal clk, button_pressed, emergency, carpark_status, front_barrier, under_barrier, after_barrier      : std_logic;
    -------------------------------------------------------------------------------
    -- Observed signals - signals mapped to the output ports of the tested entity
    -------------------------------------------------------------------------------	
    signal barrier_position, print_ticket, car_passed    : std_logic;
	begin
		-----------------------------------------------
		-- Unit Under Test instantiation and port map
		-----------------------------------------------
		UUT : entry_entity
			port map (
			clk => clk,
			button_pressed => button_pressed,
			emergency => emergency,
			carpark_status	 => carpark_status,
			front_barrier => front_barrier,
			under_barrier => under_barrier,
			after_barrier => after_barrier,
			barrier_position => barrier_position,		
			print_ticket => print_ticket,
			car_passed => car_passed			
			);
		---------------------------------
		-- stimulus process (UUT inputs)
		---------------------------------
		stimulus : process is
		begin
			-- Check that state wont change unless correct input.  Predicted state : 0
			button_pressed <= '0'; emergency <= '0'; carpark_status <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND print_ticket = '0' AND car_passed = '0' report ERR0 severity failure;
			-- Predicted state : 0
			button_pressed <= '1'; emergency <= '0'; carpark_status <= '1'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '1';
			wait for T_CHANGE;
			assert barrier_position = '0' AND print_ticket = '0' AND car_passed = '0' report ERR0 severity failure;
			
			-- Ensure emergency is functional.  Predicted state change : 0 -> 4
			button_pressed <= '0'; emergency <= '1'; carpark_status <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND print_ticket = '0' AND car_passed = '0' report ERR4 severity failure;			
			-- Appearance of car has no effect on upper FSM since in state of emergency.  Predicted state : 4
			button_pressed <= '0'; emergency <= '1'; carpark_status <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND print_ticket = '0' AND car_passed = '0' report ERR4 severity failure;
			-- Ensure since car present, system when coming out of emergency goes to car_waiting state.  Predicted state change : 4 -> 1
			button_pressed <= '0'; emergency <= '0'; carpark_status <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND print_ticket = '0' AND car_passed = '0' report ERR1 severity failure;

			-- Run through
			-- Place the system in the no_car state.  Predicted state : 0
			button_pressed <= '0'; emergency <= '0'; carpark_status <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND print_ticket = '0' AND car_passed = '0' report ERR0 severity failure;
			-- Car appears at the front barrier.  Predicted state change of barrier : 0 -> 1; Predicted state : 0
			button_pressed <= '0'; emergency <= '0'; carpark_status <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND print_ticket = '0' AND car_passed = '0' report ERR0 severity failure;
			-- Actual update of this state machine on second cycle.  Predicted state change : 0 -> 1
			button_pressed <= '0'; emergency <= '0'; carpark_status <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND print_ticket = '0' AND car_passed = '0' report ERR1 severity failure;
			-- Attempt to transition to printing_ticket, shouldn't work because car park is full.  Predicted state : 1
			button_pressed <= '1'; emergency <= '0'; carpark_status <= '1'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND print_ticket = '0' AND car_passed = '0' report ERR1 severity failure;
			-- Transition to printing_ticket, should work as car park is not full.  Predicted state change of this : 1 -> 2
			button_pressed <= '1'; emergency <= '0'; carpark_status <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '1' AND print_ticket = '1' AND car_passed = '0' report ERR1 severity failure;
			-- Simulate car still waiting for the barrier to rise.  Predicted state : 2
			button_pressed <= '0'; emergency <= '0'; carpark_status <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '1' AND print_ticket = '1' AND car_passed = '0' report ERR2 severity failure;
			-- Should still be in printing state, waiting for car to pass right the way through.  Predicted state : 2
			button_pressed <= '0'; emergency <= '0'; carpark_status <= '0'; front_barrier <= '0'; under_barrier <= '1'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '1' AND print_ticket = '1' AND car_passed = '0' report ERR2 severity failure;
    		-- Car detected under and after the barrier; no state change should occur.  Predicted state : 2
			button_pressed <= '0'; emergency <= '0'; carpark_status <= '0'; front_barrier <= '0'; under_barrier <= '1'; after_barrier <= '1';
			wait for T_CHANGE;
			assert barrier_position = '1' AND print_ticket = '1' AND car_passed = '0' report ERR2 severity failure;
			-- Transition to car_through stage. Predicted state change of this : 2 -> 3
			button_pressed <= '0'; emergency <= '0'; carpark_status <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '1';
			wait for T_CHANGE;
			assert barrier_position = '1' AND print_ticket = '0' AND car_passed = '1' report ERR3 severity failure;			
			-- Back to no car state.  Predicted state change of barrier: 2 -> 0
			button_pressed <= '0'; emergency <= '0'; carpark_status <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND print_ticket = '0' AND car_passed = '0' report ERR3 severity failure;
			--Predicted state change of this : 3 -> 0
			wait for T_CHANGE;
			assert barrier_position = '0' AND print_ticket = '0' AND car_passed = '0' report ERR0 severity failure;
			
			-- Run through again; emergency in middle.
			-- Remain in car waiting state.  Predicted state : 0
			button_pressed <= '0'; emergency <= '0'; carpark_status <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND print_ticket = '0' AND car_passed = '0' report ERR0 severity failure;
			-- Car appears at front barrier.  Predicted state change of barrier : 0 -> 1; Predicted state : 0
			button_pressed <= '0'; emergency <= '0'; carpark_status <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND print_ticket = '0' AND car_passed = '0' report ERR0 severity failure;
			-- Actual update of this state machine on second cycle.  Predicted state change : 0 -> 1
			button_pressed <= '0'; emergency <= '0'; carpark_status <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND print_ticket = '0' AND car_passed = '0' report ERR1 severity failure;
			-- Transition to printing_ticket in two steps, should work as car park is not full.  Predicted state change of this : 1 -> 2
			button_pressed <= '1'; emergency <= '0'; carpark_status <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '1' AND print_ticket = '1' AND car_passed = '0' report ERR1 severity failure;
			-- Simulate car waiting for barrier to rise.  Predicted state : 2
			button_pressed <= '0'; emergency <= '0'; carpark_status <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0'; 
			wait for T_CHANGE;
			assert barrier_position = '1' AND print_ticket = '1' AND car_passed = '0' report ERR2 severity failure;
			--  Car detected under and after the barrier; no state change should occur.  Predicted state : 2
			button_pressed <= '0'; emergency <= '0'; carpark_status <= '0'; front_barrier <= '0'; under_barrier <= '1'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '1' AND print_ticket = '1' AND car_passed = '0' report ERR2 severity failure;	
			
			--EMERGENCY!! Predicted state change : 2 -> 4
			button_pressed <= '0'; emergency <= '1'; carpark_status <= '0'; front_barrier <= '0'; under_barrier <= '1'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '1' AND print_ticket = '0' AND car_passed = '0' report ERR4 severity failure;	
			-- Remain in emergency.  Car backtracks, so barrier will be allowed to fall in sub-system.  Predicted state : 4
			button_pressed <= '0'; emergency <= '1'; carpark_status <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND print_ticket = '0' AND car_passed = '0' report ERR4 severity failure;	
			--Long emergency.  Predicted state : 4
			button_pressed <= '0'; emergency <= '1'; carpark_status <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND print_ticket = '0' AND car_passed = '0' report ERR4 severity failure;				
			--Panic over.  No car is waiting so go back to the no car state.  Predicted state change : 4 -> 0
			button_pressed <= '0'; emergency <= '0'; carpark_status <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';  -- Init state: State 0
			wait for T_CHANGE;
			assert barrier_position = '0' AND print_ticket = '0' AND car_passed = '0' report ERR0 severity failure;	
			wait;
		end process;
	
	
	------------------------------
    -- clock generation process
    ------------------------------

    clk_generator : process is          
    begin
        while now <= 40*T_clk loop		-- define number of clock cycles to simulate
        
			clk <= '1';					-- create one full clock period every loop iteration
            wait for T_clk/2;
			clk <= '0';
            wait for T_clk/2;
			
        end loop;	   
		
		wait;		
        
    end process	clk_generator;
	
end architecture;

