library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity entry_entity is
    port(
		clk         		: in  std_logic; --clock
		-- Input from button to retrieve ticket
		button_pressed		: in std_logic;
		-- Input from higher level to inform system of emergency state,
		-- where '0' indicates no emergency
		emergency			: in std_logic;
		-- Input from higher level to inform system if car park is full,
		-- where '0' indicates that it is not full
		carpark_status		: in std_logic; 
		-- The barrier sensors; neccessary for testbench purposes.
		front_barrier, under_barrier, after_barrier  : in  std_logic; 
		
		-- Output showing current barrier position (testing purposes)
		barrier_position	: out std_logic; 
		print_ticket		: out std_logic; -- Output to ticket printer
		-- Output to higher level which informs it that a car has entered
		car_passed			: out std_logic
    );
end entry_entity;

architecture entry of entry_entity is
	
	type entry_state is (
		no_car, -- Initial state
		car_waiting,
		printing_ticket,
		car_through,
		state_of_emergency
	);
	
	signal current_state : entry_state; -- Signal to track current state
	-- Signals which take input from barrier component 
	signal car_at_barrier, car_passed_barrier : std_logic;
	-- Signal which provides output to barrier component
	signal action_on_barrier : std_logic; 
	
	-- Declare the barrier component
	component barrier_entity is
		port(
			clk         : in  std_logic; --clock
			-- input from upper layer to signal an action should occur on the barrier
			action_barrier      : in  std_logic; 
			 -- the barrier sensors
			front_barrier, under_barrier, after_barrier  : in  std_logic;
			
			--output to the barrier to get it to raise or lower
			effect_barrier		: out std_logic;
			--output to system to determine if there is a car at front sensor
			at_barrier   		: out std_logic;
			 --output to system to say car is beyond barrier
			passed_barrier   	: out std_logic
		);
	end component;
	
	-- Declare which barrier architecture to use
	for barrier : barrier_entity use entity work.barrier_entity(barrier);
	
	begin
	-- Do a direct port map for clarity, need to map all the component inputs
		barrier : barrier_entity
			port map( 	clk => clk,
						action_barrier => action_on_barrier,
						front_barrier => front_barrier,
						under_barrier => under_barrier,
						after_barrier => after_barrier,
						effect_barrier => barrier_position,
						at_barrier => car_at_barrier,
						passed_barrier => car_passed_barrier
						);
		
		-- This is the process which changes the current state
		update_state : process (clk, emergency)
		 -- Locally store the next state in the sequence
			variable next_state : entry_state;
			
			begin
				if emergency = '1' then
					current_state <= state_of_emergency;
					
				elsif rising_edge(clk) then
					case current_state is
						when no_car =>
						-- Check if there is a car at barrier
							if car_at_barrier = '1' then
								next_state := car_waiting;
							else	-- Otherwise, do nothing.
								next_state := no_car;	
							end if;
							
						when car_waiting =>
						-- Ensure car is still there
							if car_at_barrier = '0' then 
								next_state := no_car;
						-- Check to see if button has been pressed and there is space
							elsif (button_pressed = '1') AND (carpark_status = '0') then	
								next_state := printing_ticket;						
							else		-- Otherwise, do nothing.
								next_state := car_waiting;	
							end if;							
							
						when printing_ticket =>
						-- Check if there is a car has passed through
							if car_passed_barrier = '1' then			
								next_state := car_through;
							elsif car_at_barrier = '0' then
								next_state := no_car;
							else		-- Otherwise, do nothing.
								next_state := printing_ticket;	
							end if;												
						
						when car_through =>
						-- Always move straight onto NoCar state 
							next_state := no_car; 
							
						when state_of_emergency =>
						-- If there's a car at the barrier after the emergency
						-- Go to the car_waiting state.
							if car_at_barrier = '1' then
								next_state := car_waiting;	
							else	-- Otherwise, go to the no_car state.
								next_state := no_car;
							end if;		
							
						when others =>	-- Safety case; do nothing.
							null;
					end case;
					
				current_state <= next_state;         -- update current_state signal at end of process						
				end if;					
		end process;
	
	-- This is the process which deals with inputs
	-- Note of caution: Potentially confusing aspect lies ahead.
	-- Remember that the output action_on_barrier is linked 
	-- directly to the underlying barrier instance and is not the value of
	-- the output effect barrier of this state machine which is instead 
	-- simply the output of the underlying barrier; this output is simply
	-- used for testing to assert proper position handling.
	assign_outputs : process (current_state) is
		begin
			case current_state is
			-- No action required.
				when no_car =>
					action_on_barrier <= '0';
					print_ticket <= '0';
					car_passed <= '0';
			-- A car is waiting.		
				when car_waiting =>
					action_on_barrier <= '0';
					print_ticket <= '0';
					car_passed <= '0';
			-- The ticket should be printed and barrier raised		
				when printing_ticket =>
					action_on_barrier <= '1';
					print_ticket <= '1';
					car_passed <= '0';
			-- Notify the system of car passage
				when car_through =>
					action_on_barrier <= '0';
					print_ticket <= '0';
					car_passed <= '1';
			-- Lock down the system
				when state_of_emergency =>
					action_on_barrier <= '0';
					print_ticket <= '0';
					car_passed <= '0';
					
				when others =>	-- Safety case; do nothing.
					null;
			end case;
			
		end process;
end architecture;

