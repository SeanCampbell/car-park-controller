library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity entry_entity is
    port(
		clk         		: in  std_logic; --clock 
		button_pressed		: in std_logic; --INput from button to retrieve ticket
		emergency			: in std_logic; --Input from higher level to inform system of emergency state, '0' indicates no emergency
		carpark_status		: in std_logic; --Input from higher level to inform system if car park is full, '0' indicates that it is not full
		front_barrier, under_barrier, after_barrier  : in  std_logic; -- the barrier sensors, we will have to make these visible to upper system I think
		
		barrier_position	: out std_logic; --Output showing current barrier position (testing purposes)
		print_ticket		: out std_logic; --Output to ticket printer
		car_passed			: out std_logic  --Output to higher level which informs it that a car has entered
    );
end entry_entity;

architecture entry of entry_entity is
	
	type entry_state is (
		no_car, --initial state
		car_waiting,
		printing_ticket,
		car_through,
		state_of_emergency
	);
	
	signal current_state : entry_state; --Signal to track current state
	 
	signal car_at_barrier, car_passed_barrier : std_logic; --Signals which take input from barrier component
	signal action_on_barrier : std_logic; --Signals which provide output to barrier component
	
	--Declare the barrier component
	component barrier_entity is
		port(
			clk         : in  std_logic; --clock 
			action_barrier      : in  std_logic; -- input from upper layer to signal an action should occur on the barrier
			front_barrier, under_barrier, after_barrier  : in  std_logic; -- the barrier sensors
			
			effect_barrier		: out std_logic; --output to the barrier to get it to raise or lower
			at_barrier   		: out std_logic; --output to system to determine if there is a car at front sensor
			passed_barrier   	: out std_logic --output to system to say car is beyond barrier
		);
	end component;
	
	--Declare which barrier architecture to use
	for barrier : barrier_entity use entity work.barrier_entity(barrier);
	
	begin
		--Do a direct port map for clarity, need to map all the component inputs		
		barrier : barrier_entity
			port map( 	clk => clk,
						action_barrier => action_on_barrier,
						front_barrier => front_barrier,
						under_barrier => under_barrier,
						after_barrier => after_barrier,
						effect_barrier => barrier_position,
						at_barrier => car_at_barrier,
						passed_barrier => car_passed_barrier
						);
		
		--This is the process which changes the current state
		update_state : process (clk, emergency)
			variable next_state : entry_state; --Locally store the next state in the sequence
			
			begin
				if emergency = '1' then
					current_state <= state_of_emergency;
					
				elsif rising_edge(clk) then
					case current_state is
						when no_car =>
							if car_at_barrier = '1' then			-- Check if there is a car at barrier
								next_state := car_waiting;
							else								-- Otherwise, do nothing.
								next_state := no_car;	
							end if;
							
						when car_waiting =>
							if car_at_barrier = '0' then -- Make sure car is still there
								next_state := no_car;
							elsif (button_pressed = '1') AND (carpark_status = '0') then	--Check to see if button has been pressed and there is space
								next_state := printing_ticket;						
							else								-- Otherwise, do nothing.
								next_state := car_waiting;	
							end if;							
							
						when printing_ticket =>
							if car_passed_barrier = '1' then			-- Check if there is a car has passed through
								next_state := car_through;
							elsif car_at_barrier = '0' then
								next_state := no_car;
							else								-- Otherwise, do nothing.
								next_state := printing_ticket;	
							end if;												
						
						when car_through =>
							next_state := no_car; --Always move straight onto next 
							
						when state_of_emergency =>
							if car_at_barrier = '1' then	-- If there's a car at the barrier after the emergency
								next_state := car_waiting;	-- Go to the car_waiting state.
							else							-- Otherwise, go to the no_car state.
								next_state := no_car;
							end if;		
							
						when others =>	-- Safety case; do nothing.
							null;
					end case;
					
				current_state <= next_state;         -- update current_state signal at end of process						
				end if;					
		end process;
	
	-- This is the process which deals with inputs
	-- Note of caution: Potentially confusing aspect lies ahead.
	-- Remember that the output action_on_barrier is linked directly to the underlying barrier instance and is not
	-- the value of the output effect barrier of this state machine which is instead simply the output of the
	-- underlying barrier; this output is simply used for testing to assert proper position handling.
	assign_outputs : process (current_state) is
		begin
			case current_state is
				when no_car =>
					action_on_barrier <= '0';
					print_ticket <= '0';
					car_passed <= '0';
					
				when car_waiting =>
					action_on_barrier <= '0';
					print_ticket <= '0';
					car_passed <= '0';
					
				when printing_ticket =>
					action_on_barrier <= '1';
					print_ticket <= '1';
					car_passed <= '0';
				
				when car_through =>
					action_on_barrier <= '0';
					print_ticket <= '0';
					car_passed <= '1';
				
				when state_of_emergency =>
					action_on_barrier <= '0';
					print_ticket <= '0';
					car_passed <= '0';
					
				when others =>	-- Safety case; do nothing.
					null;
			end case;
			
		end process;
		
end architecture entry;

