library ieee;
use ieee.std_logic_1164.all;

entity exit_lane_tb is
end entity exit_lane_tb;

architecture v1 of exit_lane_tb is
	
	
	--Component being tested
	component exit_entity is
		port(
			clk         : in  std_logic; --clock 
			ticketInserted		: in std_logic; --INput from button to retrieve ticket
			emergency			: in std_logic; --Input from higher level to inform system of emergency state, '0' indicates no emergency
			validTicket    : in std_logic; --input from higher level stating if the ticket is valid
			front_barrier, under_barrier, after_barrier  : in  std_logic; -- the barrier sensors, we will have to make these visible to upper system I think
			
			barrier_position     : out std_logic; --output to the barrier to get it to raise or lower		
			validate_ticket		: out std_logic; --Output to ticket printer
			car_passed			: out std_logic --Output to higher level which informs it that a car has entered
		);
	end component;
	
	--------------------------------------
    -- Constants used within the design
    --------------------------------------
    constant T_clk      : time    := 10 ns;	-- define clock period
	constant T_CHANGE : time := 11 ns;
	constant ERR0 : string := "Predicted State 0 output violation";
	constant ERR1 : string := "Predicted State 1 output violation";
	constant ERR2 : string := "Predicted State 2 output violation";
	constant ERR3 : string := "Predicted State 3 output violation";
	constant ERR4 : string := "Predicted State 4 output violation";
	constant ERR5 : string := "Predicted State 5 output violation";
	------------------------------------------------------------------------------
    -- Stimulus signals - signals mapped to the input ports of the tested entity
    ------------------------------------------------------------------------------
    signal clk, ticketInserted, validTicket, emergency, front_barrier, under_barrier, after_barrier      : std_logic;
    -------------------------------------------------------------------------------
    -- Observed signals - signals mapped to the output ports of the tested entity
    -------------------------------------------------------------------------------
    signal barrier_position, validate_ticket, car_passed    : std_logic;
	
	begin
		-----------------------------------------------
		-- Unit Under Test instantiation and port map
		-----------------------------------------------
		UUT : exit_entity
			port map (
			clk => clk,
			ticketInserted => ticketInserted,
			emergency => emergency,
			validTicket => validTicket,
			front_barrier => front_barrier,
			under_barrier => under_barrier,
			after_barrier => after_barrier,
			barrier_position => barrier_position,		
			validate_ticket => validate_ticket,
			car_passed => car_passed			
			);
		---------------------------------
		-- stimulus process (UUT inputs)
		---------------------------------
		stimulus : process is
		begin
		
			-- Check that state wont change unless correct input.  Predicted state change : 0 -> 4
			ticketInserted <= '0'; emergency <= '0'; validTicket<='0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND validate_ticket = '0' AND car_passed = '0' report ERR0 severity failure;
			-- Predicted state : 0
			ticketInserted <= '1'; emergency <= '0';validTicket<='1'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '1';
			wait for T_CHANGE;
			assert barrier_position = '0' AND validate_ticket = '0' AND car_passed = '0' report ERR0 severity failure;
			
			-- Esnure emergency works.  Predicted state change : 0 -> 4
			ticketInserted <= '0'; emergency <= '1'; validTicket<='0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';  -- Init state: State 0
			wait for T_CHANGE;
			assert barrier_position = '1' AND validate_ticket = '0' AND car_passed = '0' report ERR5 severity failure;			
			-- Appearance of car has no effect on upper FSM since in state of emergency.  Predicted state : 5
			ticketInserted <= '0'; emergency <= '1'; validTicket <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '1' AND validate_ticket = '0' AND car_passed = '0' report ERR5 severity failure;
			-- Car travels through the exit.  Ensure we are notified about the car leaving.  Predicted state change : 5 -> 4
			ticketInserted <= '0'; emergency <= '1'; validTicket <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '1';
			wait for T_CHANGE;
			assert barrier_position = '1' AND validate_ticket = '0' AND car_passed = '1' report ERR4 severity failure;
			-- Ensure system returns to the emergency state.  Predicted state change : 4 -> 5		
			ticketInserted <= '0'; emergency <= '1'; validTicket<='0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';  -- Init state: State 0
			wait for T_CHANGE;
			assert barrier_position = '1' AND validate_ticket = '0' AND car_passed = '0' report ERR5 severity failure;	
			-- Ensure system returns to normal.
			ticketInserted <= '0'; emergency <= '0'; validTicket<='0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND validate_ticket = '0' AND car_passed = '0' report ERR0 severity failure;
			-- Double check system doesn't bounce to the waiting state.
			ticketInserted <= '0'; emergency <= '0'; validTicket<='0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND validate_ticket = '0' AND car_passed = '0' report ERR0 severity failure;

			-- Normal run though of the system.
			-- The system is in the normal start state.
			ticketInserted <= '0'; emergency <= '0'; validTicket <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND validate_ticket = '0' AND car_passed = '0' report ERR0 severity failure;
			-- A vehicle appear at the front sensor.
			ticketInserted <= '0'; emergency <= '0'; validTicket <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND validate_ticket = '0' AND car_passed = '0' report ERR0 severity failure;
			-- The ticket is inserted by the driver.  The system considers the ticket.
			ticketInserted <= '1'; emergency <= '0'; validTicket <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND validate_ticket = '1' AND car_passed = '0' report ERR0 severity failure;
			-- The ticket is perceived as being valid.  Delay time cycles to allow state transitions.
			ticketInserted <= '1'; emergency <= '0'; validTicket <= '1'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '0' AND validate_ticket = '0' AND car_passed = '0' report ERR0 severity failure;
			-- The barrier position will be updated.
			ticketInserted <= '1'; emergency <= '0'; validTicket <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '1' AND validate_ticket = '0' AND car_passed = '0' report ERR0 severity failure;
			-- The driver desperate after all these delays rushes through the barriers.
			ticketInserted <= '0'; emergency <= '0'; validTicket <= '0'; front_barrier <= '0'; under_barrier <= '1'; after_barrier <= '0';
			wait for T_CHANGE;
			assert barrier_position = '1' AND validate_ticket = '0' AND car_passed = '0' report ERR0 severity failure;
			-- The driver is under both barriers.
			ticketInserted <= '1'; emergency <= '0'; validTicket <= '0'; front_barrier <= '0'; under_barrier <= '1'; after_barrier <= '1';
			wait for T_CHANGE;
			assert barrier_position = '1' AND validate_ticket = '0' AND car_passed = '0' report ERR0 severity failure;
			-- The driver has left the car park completely; should be informed of departure.
			ticketInserted <= '1'; emergency <= '0'; validTicket <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '1';
			wait for T_CHANGE;
			assert barrier_position = '1' AND validate_ticket = '0' AND car_passed = '1' report ERR0 severity failure;
			-- System returns to normal.
			ticketInserted <= '1'; emergency <= '0'; validTicket <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '1';
			wait for T_CHANGE;
			assert barrier_position = '0' AND validate_ticket = '0' AND car_passed = '0' report ERR0 severity failure;
			wait;
			
		end process;
	
	------------------------------
    -- clock generation process
    ------------------------------

    clk_generator : process is          
    begin
        while now <= 30*T_clk loop		-- define number of clock cycles to simulate
        
			clk <= '1';					-- create one full clock period every loop iteration
            wait for T_clk/2;
			clk <= '0';
            wait for T_clk/2;
			
        end loop;	   
		
		wait;		
        
    end process	clk_generator;
	
end architecture;

