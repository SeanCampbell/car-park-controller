library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity barrier_entity is
    port(
		clk         : in  std_logic; --clock 
        action_barrier      : in  std_logic; -- input from upper layer to signal an action should occur on the barrier
      	front_barrier, under_barrier, after_barrier  : in  std_logic; -- the barrier sensors
		
        effect_barrier     : out std_logic; --output to the barrier to get it to raise or lower
        at_barrier   : out std_logic; --output to system to determine if there is a car at front sensor
        passed_barrier   : out std_logic --output to system to say car is beyond barrier
    );
end barrier_entity;

architecture barrier of barrier_entity is
    type barrier_state is (
        BarrierDown,
        CarParkStatus,
        BarrierUp,
		ChangeStatus
    );
    signal current_state : barrier_state;

	begin
    update_state : process (clk)
		variable next_state : barrier_state;
		
		begin
        if rising_edge(clk) then
			case current_state is
				when BarrierDown =>
					if action_barrier = '1' then	-- If action required, go to barrier up.
						next_state := BarrierUp;
					else
						if front_barrier = '1' then	-- Check front barrier sensor.  If one proceed.
							next_state := CarParkStatus;
						else
							next_state := BarrierDown; -- If action required and no car detected, inferred emergency.
						end if;
				end if;
				
				when CarParkStatus =>
					if action_barrier = '1' then -- If spaces available or emergency, open the barrier.
						next_state := BarrierUp;
					else
						if front_barrier = '1' then	-- If no action and car still at barrier, remain in state.
							next_state := CarParkStatus; -- This means no emergency but car park full.
						else						-- If car has left the barrier, go back to initial state.
							next_state := BarrierDown;	
						end if;
					end if;
			
				when BarrierUp =>
					if action_barrier = '1' then
						if front_barrier = '1' OR under_barrier = '1' then -- If car detected either position then
							next_state := BarrierUp;	-- keep the barrier up.
						elsif after_barrier = '1' then -- If car only detected after the barrier then
							next_state := ChangeStatus;	-- Signify the car park status has changed.
						else	-- Else inferred that state entered via state of emergency then
							next_state := BarrierUp;	-- Remain up.
						end if;
					else	-- Know we want to close the barrier but first check if there is a car underneath it.
						if under_barrier = '1' then	-- If car under the barrier allow it to get through.
							next_state := BarrierUp;
						else						-- Otherwise put the barrier down.
							next_state := BarrierDown;
						end if;
					end if;
				
				-- This state has the advantage of allowing counting even while in a state of emergency.
				when ChangeStatus =>
					if action_barrier = '1' then -- If action to occur (inferred emergency)
						next_state := BarrierUp; -- Then go back to keeping the barrier up.
					else
						next_state := BarrierDown; -- Otherwise go back to keeping the barrier down.
					end if;
					
				when others =>	-- Safety case; do nothing.
					null;

			end case;
					
            current_state <= next_state;         -- update current_state signal at end of process
            end if;
    end process;


    assign_outputs : process (current_state) is
    begin
        case current_state is
            when BarrierDown =>
				effect_barrier <= '0';	-- No signals output if nothing occurring.
				at_barrier <= '0';
				passed_barrier <='0';

            when CarParkStatus =>			
				effect_barrier <= '0'; -- Car waiting so query status.
				at_barrier <= '1';
				passed_barrier <='0';

            when BarrierUp =>
				effect_barrier <= '1'; -- Raise the barrier.
				at_barrier <= '1';
				passed_barrier <='0';

            when ChangeStatus =>
				effect_barrier <= '1'; -- Keep the barrier raised.
				at_barrier <= '0';
				passed_barrier <='1';  -- Update the status of the car park.
				
			when others =>	-- Safety case; do nothing.
				null;
        end case;

    end process;

end barrier;

