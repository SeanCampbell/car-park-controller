library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity barrier_entity is
    port(
		clk         : in  std_logic; --clock 
		-- input from upper layer to signal an action should occur on
		-- the barrier
        action_barrier      : in  std_logic;
		 -- the barrier sensors
      	front_barrier, under_barrier, after_barrier  : in  std_logic;
		
		 --output to the barrier to get it to raise or lower
        effect_barrier     : out std_logic;
		 --output to system to determine if there is a car at front sensor
        at_barrier   : out std_logic;
		--output to system to say car is beyond barrier
        passed_barrier   : out std_logic 
    );
end barrier_entity;

architecture barrier of barrier_entity is
    type barrier_state is (
        BarrierDown,
        CarParkStatus,
        BarrierUp,
		ChangeStatus
    );
    signal current_state : barrier_state;

	begin
    update_state : process (clk)
		variable next_state : barrier_state;
		begin
		-- If clock is on the falling change,
		if falling_edge(clk) then
			-- If the upper system wants an action to occur, this takes priority.
			if action_barrier = '1' then
			-- If theres something under the barrier remain up.
				if under_barrier = '1' then
					current_state <= BarrierUp;
			-- If theres something after, switch for a clock cycle to output
			-- that something has passed.
				elsif after_barrier = '1' then
					current_state <= ChangeStatus;
				else
					current_state <= BarrierUp;
				end if;
			else
				case current_state is
				when BarrierDown =>
					-- Check front barrier sensor.  If one proceed.
					if front_barrier = '1' then	
						next_state := CarParkStatus;
					else
						next_state := BarrierDown; -- If action required
					end if;		-- and no car detected, inferred emergency.
				
				when CarParkStatus =>
					-- Remain in state if car still there since querying upper system.
					if front_barrier = '1' then	
						next_state := CarParkStatus; 
					else	-- If the car has left, go back to initial state.
						next_state := BarrierDown;	
					end if;
			
				when BarrierUp =>
					-- Know we want to close the barrier but first check
					-- if there is a car underneath it.
					-- If car under the barrier allow it to get through.
					if under_barrier = '1' then	
						next_state := BarrierUp;
					else				-- Otherwise put the barrier down.
						next_state := BarrierDown;
					end if;

				when ChangeStatus =>
						next_state := BarrierDown; -- Proceed immediately to Down.
					
				when others =>	-- Safety case; do nothing.
					null;

			end case;
            current_state <= next_state;
			-- update current_state signal at end of conditional.
			end if;
		end if;
    end process;

    assign_outputs : process (current_state) is
    begin
        case current_state is
			-- No signals output if nothing occurring.
            when BarrierDown =>
				effect_barrier <= '0';
				at_barrier <= '0';
				passed_barrier <='0';
			-- Car waiting so query status.
            when CarParkStatus =>			
				effect_barrier <= '0';
				at_barrier <= '1';
				passed_barrier <='0';
			 -- Raise the barrier.
            when BarrierUp =>
				effect_barrier <= '1';
				at_barrier <= '1';
				passed_barrier <='0';
			-- Keep the barrier raised and update the car park status.
            when ChangeStatus =>
				effect_barrier <= '1';
				at_barrier <= '0';
				passed_barrier <='1';  
				
			when others =>	-- Safety case; do nothing.
				null;
        end case;

    end process;

end barrier;

