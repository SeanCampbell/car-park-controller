library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity controller_tb is
end entity;

architecture controller_v1 of controller_tb is
	--------------------------------------
    -- Constants used within the design
    --------------------------------------
    constant T_clk      : time  	:= 10 ns;	-- define clock period
	constant T_CHANGE 	: time 		:= 11 ns;
	
	constant ENTRY_BITS : positive := 2;
	constant EXIT_BITS : positive := 2;
	constant SPACES_BITS : positive := 2;
	constant MAX_CAP : positive := 3;
	constant MSG : string := "Output Violation";
	--------------------------------------
    -- Component employed in the testbench
    --------------------------------------
	component controller_entity is
		generic( entry_bit_count : positive := 2; -- Number of bits for the entrances; default of four entrances.
				exit_bit_count : positive := 2; -- Number of bits for the exits; default of four exits.
				spaces_bit_count : positive := 4; -- Number of bits for the car park spaces; default of thrity-two spaces.
				max_count : positive := 10		-- The actual number of spaces allowed.  These two numbers are linked.
		);
		 port(
		-- This specific inputs.
		clk         : in  std_logic; -- The system clock.
		emergency 	: in std_logic;  -- The emergency input.
		-- The entrance sensor arrays.
		entry_front_sensors : in std_logic_vector(0 to entry_bit_count-1);
		entry_under_sensors : in std_logic_vector(0 to entry_bit_count-1);
		entry_after_sensors : in std_logic_vector(0 to entry_bit_count-1);
		entry_button_presses : in std_logic_vector(0 to entry_bit_count-1);
		-- The exit sensor arrays.
		exit_front_sensors : in std_logic_vector(0 to exit_bit_count-1);
		exit_under_sensors : in std_logic_vector(0 to exit_bit_count-1);
		exit_after_sensors : in std_logic_vector(0 to exit_bit_count-1);
		exit_inserted_tickets : in std_logic_vector(0 to entry_bit_count-1);
		exit_valid_tickets : in std_logic_vector(0 to entry_bit_count-1);
		-- The entrance outputs.
		entry_barrier_pos : out std_logic_vector(0 to entry_bit_count-1);
		entry_car_entered : out std_logic_vector(0 to entry_bit_count-1);
		entry_print_tickets : out std_logic_vector(0 to entry_bit_count-1);
		-- The exit outputs.
		exit_barrier_pos : out std_logic_vector(0 to exit_bit_count-1);
		exit_car_entered : out std_logic_vector(0 to exit_bit_count-1);
		exit_validate_tickets : out std_logic_vector(0 to exit_bit_count-1);
		-- This specific outputs.
		status : out std_logic; -- Output informing the lower system
		count : out unsigned(spaces_bit_count-1 downto 0)
    );
	end component controller_entity;
	------------------------------------------------------------------------------
    -- Stimulus signals - signals mapped to the input ports of the tested entity
    ------------------------------------------------------------------------------
    signal clk, emergency : std_logic;
	signal entry_front_sensors, entry_under_sensors, entry_after_sensors,
		entry_button_presses : std_logic_vector(0 to ENTRY_BITS-1);
	signal exit_front_sensors, exit_under_sensors, exit_after_sensors,
			exit_inserted_tickets, exit_valid_tickets : std_logic_vector(0 to EXIT_BITS-1);
    -------------------------------------------------------------------------------
    -- Observed signals - signals mapped to the output ports of the tested entity
    -------------------------------------------------------------------------------
	signal status : std_logic;
	signal entry_barrier_pos, entry_car_entered, entry_print_tickets : std_logic_vector(0 to ENTRY_BITS-1);
	signal exit_barrier_pos, exit_car_entered, exit_validate_tickets : std_logic_vector(0 to EXIT_BITS-1);
	signal count : unsigned(SPACES_BITS-1 downto 0);
	
	begin
	-----------------------------------------------
    -- Unit Under Test instantiation and port map
    -----------------------------------------------
    UUT : controller_entity
	generic map(
		entry_bit_count => ENTRY_BITS,
		exit_bit_count => EXIT_BITS,
		spaces_bit_count => SPACES_BITS,
		max_count => MAX_CAP
	) port map (
        clk => clk,
		emergency => emergency,
        entry_front_sensors => entry_front_sensors,
        entry_under_sensors => entry_under_sensors,
        entry_after_sensors => entry_after_sensors,
        entry_button_presses => entry_button_presses,
        exit_front_sensors => exit_front_sensors,
        exit_under_sensors => exit_under_sensors,
		exit_after_sensors => exit_after_sensors,
		exit_inserted_tickets => exit_inserted_tickets,
		exit_valid_tickets => exit_valid_tickets,
		entry_barrier_pos => entry_barrier_pos,
		entry_car_entered => entry_car_entered,
		entry_print_tickets => entry_print_tickets,
		exit_barrier_pos => exit_barrier_pos,
		exit_car_entered => exit_car_entered,
		exit_validate_tickets => exit_validate_tickets,
		status => status,
		count => count
		);				
    ---------------------------------
    -- stimulus process (UUT inputs)
    ---------------------------------
    stimulus : process is
    begin
		-- Do a normal run through.
		-- The system should be in its a peaceful state.  No actions are occurring.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "11" report MSG severity failure;
		-- A car appears at one of the entry sensors.  One time cycle for underlying FSMs to change.
		emergency <= '0';
		entry_front_sensors <= "10"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "11" report MSG severity failure;
		-- The car remains at the entrance.
		emergency <= '0';
		entry_front_sensors <= "10"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "11" report MSG severity failure;
		-- The car remains at the entrance and the driver pushes the button.  The entry should print the ticket and
		-- the barrier should open at the next point in time.
		emergency <= '0';
		entry_front_sensors <= "10"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "10";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "10" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "11" report MSG severity failure;
			
		emergency <= '0';
		entry_front_sensors <= "10"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "10";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "10" AND entry_car_entered = "00" AND entry_print_tickets = "10" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "11" report MSG severity failure;	
		-- The barrier is open and the driver, eager to get in, goes through the entrance.  Check the barrier doesn't close.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "10"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "10" AND entry_car_entered = "00" AND entry_print_tickets = "10" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "11" report MSG severity failure;	
		-- The barrier should still be open and the vehicle is under the barrier.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "10"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "10" AND entry_car_entered = "00" AND entry_print_tickets = "10" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "11" report MSG severity failure;	
		-- The barrier should still be open and the vehicle is under the barrier and after the barrier.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "10"; entry_after_sensors <= "10"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "10" AND entry_car_entered = "00" AND entry_print_tickets = "10" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "11" report MSG severity failure;			
		-- The car should be successfully into the car park.  Shold mark car as entering and
		-- the count should update.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "00"; entry_after_sensors <= "10"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "10" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "10" report MSG severity failure;
		-- The system should be in the original state.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "10" report MSG severity failure;	
		
		-- Repeat the process.  Check the count updates properly.
		-- The system should be in its peaceful state.  No actions are occurring.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "10" report MSG severity failure;
		-- A car appears at one of the entry sensors.  One time cycle for underlying FSMs to change.
		emergency <= '0';
		entry_front_sensors <= "10"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "10" report MSG severity failure;
		-- The car remains at the entrance.
		emergency <= '0';
		entry_front_sensors <= "10"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "10" report MSG severity failure;
		-- The car remains at the entrance and the driver pushes the button.  The entry should print the ticket and
		-- the barrier should open at the next point in time.
		emergency <= '0';
		entry_front_sensors <= "10"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "10";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "10" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "10" report MSG severity failure;
		emergency <= '0';
		entry_front_sensors <= "10"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "10";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "10" AND entry_car_entered = "00" AND entry_print_tickets = "10" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "10" report MSG severity failure;	
		-- The barrier is open and the driver, eager to get in, goes through the entrance.  Check the barrier doesn't close.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "10"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "10" AND entry_car_entered = "00" AND entry_print_tickets = "10" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "10" report MSG severity failure;	
		-- The barrier should still be open and the vehicle is under the barrier.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "10"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "10" AND entry_car_entered = "00" AND entry_print_tickets = "10" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "10" report MSG severity failure;	
		-- The barrier should still be open and the vehicle is under the barrier and after the barrier.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "10"; entry_after_sensors <= "10"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "10" AND entry_car_entered = "00" AND entry_print_tickets = "10" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "10" report MSG severity failure;			
		-- The car should be successfully into the car park.  Should mark car as entering and
		-- the count should update.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "00"; entry_after_sensors <= "10"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "10" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "01" report MSG severity failure;
		-- The system should be in the original state.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "01" report MSG severity failure;	
		-- Now check to see if a car is allowed to actually leave.
		-- The system should be in its peaceful state.  No actions are occurring.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "01" report MSG severity failure;
		-- A car appears at one of the exit sensors.  One time cycle for underlying FSMs to change.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "10"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "01" report MSG severity failure;
		-- The car remains at the exit.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "10"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "01" report MSG severity failure;
		-- The car remains at the exit and the driver inserts the ticket into the machine.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "10"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "10";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "10" AND
			status = '0' AND count = "01" report MSG severity failure;
		-- The car remains at the exit and the driver inserts the ticket into the machine.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "10"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "10";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "10" AND
			status = '0' AND count = "01" report MSG severity failure;
		-- The ticket is perceived as being valid.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "10"; entry_under_sensors <= "00"; exit_after_sensors <= "00"; exit_inserted_tickets <= "10";
		exit_valid_tickets <= "10";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "10" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "01" report MSG severity failure;	
		-- The driver is desperate to leave the car park at this point.	
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "10"; exit_after_sensors <= "00"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "10" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "01" report MSG severity failure;	
		-- The driver is between the final two sensors.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "10"; exit_after_sensors <= "10"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "10" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "01" report MSG severity failure;	
		-- The driver has officially left the car park and the count should be updated.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "10"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "10" AND exit_car_entered = "10" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "10" report MSG severity failure;
		-- The system should return to a normal state.
		emergency <= '0';
		entry_front_sensors <= "00"; entry_under_sensors <= "00"; entry_after_sensors <= "00"; entry_button_presses <= "00";
		exit_front_sensors <= "00"; entry_under_sensors <= "00"; exit_after_sensors <= "10"; exit_inserted_tickets <= "00";
		exit_valid_tickets <= "00";
		wait for T_CHANGE;
		assert entry_barrier_pos = "00" AND entry_car_entered = "00" AND entry_print_tickets = "00" AND
			exit_barrier_pos = "00" AND exit_car_entered = "00" AND exit_validate_tickets = "00" AND
			status = '0' AND count = "10" report MSG severity failure;
		wait;
    end process;
	------------------------------
    -- clock generation process
    ------------------------------
	clk_generator : process is          
    begin
        while now <= 200*T_clk loop		-- define number of clock cycles to simulate
        
			clk <= '1';					-- create one full clock period every loop iteration
            wait for T_clk/2;
			clk <= '0';
            wait for T_clk/2;
			
        end loop;	   
		
		wait;		
        
    end process	clk_generator;
	
end architecture;