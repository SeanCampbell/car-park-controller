library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity controller_tb is
end entity;

architecture controller_v1 of controller_tb is
	--------------------------------------
    -- Constants used within the design
    --------------------------------------
    constant T_clk      : time  	:= 10 ns;	-- define clock period
	constant T_CHANGE 	: time 		:= 11 ns;
	
	constant ENTRY_BITS : positive := 1;
	constant EXIT_BITS : positive := 1;
	constant SPACES_BITS : positive := 2;
	constant MAX_CAP : positive := 3;
	--------------------------------------
    -- Component employed in the testbench
    --------------------------------------
	component controller is
		generic( entry_bit_count : positive := 2; -- Number of bits for the entrances; default of four entrances.
				exit_bit_count : positive := 2; -- Number of bits for the exits; default of four exits.
				spaces_bit_count : positive := 4; -- Number of bits for the car park spaces; default of thrity-two spaces.
				max_count : positive := 10		-- The actual number of spaces allowed.  These two numbers are linked.
		);
		port(
			-- This specific inputs.
			clk         : in  std_logic; -- The system clock.
			emergency 	: in std_logic;  -- The emergency input.
			-- The entrance sensor arrays.
			entry_front_sensors : in std_logic_vector(0 to entry_bit_count-1);
			entry_under_sensors : in std_logic_vector(0 to entry_bit_count-1);
			entry_after_sensors : in std_logic_vector(0 to entry_bit_count-1);
			entry_button_presses : in std_logic_vector(0 to entry_bit_count-1);
			-- The exit sensor arrays.
			exit_front_sensors : in std_logic_vector(0 to exit_bit_count-1);
			exit_under_sensors : in std_logic_vector(0 to exit_bit_count-1);
			exit_after_sensors : in std_logic_vector(0 to exit_bit_count-1);
			--exit_tickets : 		 in std_logic_vector(0 to entry_bit_count-1);
			-- The entrance outputs.
			entry_barrier_pos : out std_logic_vector(0 to entry_bit_count-1);
			entry_car_entered : out std_logic_vector(0 to entry_bit_count-1);
			-- The exit outputs.
			exit_barrier_pos : out std_logic_vector(0 to entry_bit_count-1);
			exit_car_entered : out std_logic_vector(0 to entry_bit_count-1);
			-- This specific outputs.
			status : out std_logic; -- Output informng the lower system
			count : out unsigned(spaces_bit_count downto 0)
		);
	end component controller;
	------------------------------------------------------------------------------
    -- Stimulus signals - signals mapped to the input ports of the tested entity
    ------------------------------------------------------------------------------
    signal clk, emergency : std_logic;
	signal entry_front_sensors, entry_under_sensors, entry_after_sensors,
		entry_button_presses : std_logic_vector(0 to ENTRY_BITS);
	signal exit_front_sensors, exit_under_sensors, exit_after_sensors,
		exit_tickets : std_logic_vector(0 to EXIT_BITS);
    -------------------------------------------------------------------------------
    -- Observed signals - signals mapped to the output ports of the tested entity
    -------------------------------------------------------------------------------
	signal status : std_logic;
	signal entry_barrier_pos, entry_car_entered : std_logic_vector(0 to ENTRY_BITS);
	signal exit_barrier_pos, exit_car_entered : std_logic_vector(0 to EXIT_BITS);
	signal count : unsigned(0 to SPACES_BITS);
	
	begin
	-----------------------------------------------
    -- Unit Under Test instantiation and port map
    -----------------------------------------------
    UUT : controller
    port map (
        clk => clk,
        entry_front_sensors => entry_front_sensors,
        entry_under_sensors => entry_under_sensors,
        entry_after_sensors => entry_after_sensors,
        entry_button_presses => entry_button_presses,
        exit_front_sensors => exit_front_sensors,
        exit_under_sensors => exit_under_sensors,
		exit_after_sensors => exit_after_sensors,
		entry_barrier_pos => entry_barrier_pos,
		entry_car_entered => entry_car_entered,
		exit_barrier_pos => exit_barrier_pos,
		exit_car_entered => exit_car_entered,
		status => status,
		count => count
		);
    ---------------------------------
    -- stimulus process (UUT inputs)
    ---------------------------------
    stimulus : process is
    begin
		-- Simulate an emergency happening in the case of an exit.
		-- No car at the barrier.  Predicted state : 0
		action_barrier <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '0' AND at_barrier = '0' AND passed_barrier = '0' report "Output1 Violation" severity failure;
		
		-- System responding to emergency decides to invoke action upon the barrier.  Predicted state change : 0 -> 2
		action_barrier <= '1'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '1' AND at_barrier = '1' AND passed_barrier = '0' report "Output2 Violation" severity failure;
		
		-- System remaining in emergency.  Predicted state : 2
		action_barrier <= '1'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '1' AND at_barrier = '1' AND passed_barrier = '0' report "Output3 Violation" severity failure;
		
		-- System no longer in emergency.  Predicted state change : 2 -> 0
		action_barrier <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '0' AND at_barrier = '0' AND passed_barrier = '0' report "Output4 Violation" severity failure;
	
		--Simulate an emergency happening in the case of an entrance.
		-- No car at the barrier.  Predicted state : 0
		action_barrier <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '0' AND at_barrier = '0' AND passed_barrier = '0' report "Output5 Violation" severity failure;
		
		-- Car appears at barrier.  Predicted state change : 0 -> 1
		action_barrier <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '0' AND at_barrier = '1' AND passed_barrier = '0' report "Output6 Violation" severity failure;
		
		-- Car waiting at barrier.  Predicted state : 1
		action_barrier <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '0' AND at_barrier = '1' AND passed_barrier = '0' report "Output7 Violation" severity failure;
		
		-- Car still waiting.  System responds with action, indicating spaces available.  Predicted state change : 1 -> 2
		action_barrier <= '1'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '1' AND at_barrier = '1' AND passed_barrier = '0' report "Output8 Violation" severity failure;
		
		-- Pretend barrier fully raised in this time period.  Car still detected at front of barrier.  Predicted state : 2
		action_barrier <= '1'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '1' AND at_barrier = '1' AND passed_barrier = '0' report "Output9 Violation" severity failure;
		
		-- The system responds to an emergency and sets the action to lower the barrier.  Predicted state change : 2 -> 0
		action_barrier <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '0' AND at_barrier = '0' AND passed_barrier = '0' report "Output10 Violation" severity failure;
		
		-- The system remains in emergency.  Go back to check car park status since car still there.  Predicted state change : 0 -> 1
		action_barrier <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '0' AND at_barrier = '1' AND passed_barrier = '0' report "Output11 Violation" severity failure;
		
		-- The system is out of emergency.  The entrance told to operate normally.  Predicted state change : 1 -> 2
		action_barrier <= '1'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '1' AND at_barrier = '1' AND passed_barrier = '0' report "Output12 Violation" severity failure;
		
		-- The driver got annoyed and decided to shoot through.  Predicted state change : 2 -> 3
		action_barrier <= '1'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '1';
		wait for T_CHANGE;
		assert effect_barrier = '1' AND at_barrier = '0' AND passed_barrier = '1' report "Output13 Violation" severity failure;
		
		-- The system goes back to initial state.  Predicted state change : 3 -> 0
		action_barrier <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '0' AND at_barrier = '0' AND passed_barrier = '0' report "Output14 Violation" severity failure;
		
		wait;
    end process;

	
	
	
	
	
	
	
	
	
	
	emergency <= '0';
	
	clk_generator : process is          
    begin
        while now <= 20*T_clk loop		-- define number of clock cycles to simulate
        
			clk <= '1';					-- create one full clock period every loop iteration
            wait for T_clk/2;
			clk <= '0';
            wait for T_clk/2;
			
        end loop;	   
		
		wait;		
        
    end process	clk_generator;
	
end architecture;