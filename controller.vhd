library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity controller_entity is
	generic( 
			-- Number of bits for the entrances; default of four entrances.
			entry_bit_count : positive := 2; 
			-- Number of bits for the exits; default of four exits.
			exit_bit_count : positive := 2;
			-- Number of bits for the car park spaces; default of sixteen spaces.
			spaces_bit_count : positive := 4;
			-- The actual number of spaces to be allowed.
			max_count : positive := 10		
	);
    port(
		-- This specific inputs.
		clk         : in  std_logic; -- The system clock.
		emergency 	: in std_logic;  -- The emergency input.
		-- The entrance sensor arrays.
		entry_front_sensors : in std_logic_vector(0 to entry_bit_count-1);
		entry_under_sensors : in std_logic_vector(0 to entry_bit_count-1);
		entry_after_sensors : in std_logic_vector(0 to entry_bit_count-1);
		entry_button_presses : in std_logic_vector(0 to entry_bit_count-1);
		-- The exit sensor arrays.
		exit_front_sensors : in std_logic_vector(0 to exit_bit_count-1);
		exit_under_sensors : in std_logic_vector(0 to exit_bit_count-1);
		exit_after_sensors : in std_logic_vector(0 to exit_bit_count-1);
		exit_inserted_tickets : in std_logic_vector(0 to entry_bit_count-1);
		exit_valid_tickets : in std_logic_vector(0 to entry_bit_count-1);
		-- The entrance outputs.
		entry_barrier_pos : out std_logic_vector(0 to entry_bit_count-1);
		entry_car_entered : out std_logic_vector(0 to entry_bit_count-1);
		entry_print_tickets : out std_logic_vector(0 to entry_bit_count-1);
		-- The exit outputs.
		exit_barrier_pos : out std_logic_vector(0 to exit_bit_count-1);
		exit_car_entered : out std_logic_vector(0 to exit_bit_count-1);
		exit_validate_tickets : out std_logic_vector(0 to exit_bit_count-1);
		-- This specific outputs.
		status : out std_logic; -- Output informing the lower system
		count : out unsigned(spaces_bit_count-1 downto 0)
    );
end entity controller_entity;

architecture controller_v1 of controller_entity is
	-- The maximum capacity of the entire car park.
	constant MAX_CAPACITY : unsigned(spaces_bit_count-1 downto 0) := to_unsigned(max_count, spaces_bit_count);
	
	signal cp_status : std_logic := '0';
	signal cars_entered : std_logic_vector(0 to entry_bit_count-1) := (others => '0');
	signal cars_left : std_logic_vector(0 to exit_bit_count-1) := (others => '0');
	
	component entry_entity is
		port(
			clk         		: in  std_logic; --clock
		-- Input from button to retrieve ticket
		button_pressed		: in std_logic;
		-- Input from higher level to inform system of emergency state,
		-- where '0' indicates no emergency
		emergency			: in std_logic;
		-- Input from higher level to inform system if car park is full,
		-- where '0' indicates that it is not full
		carpark_status		: in std_logic; 
		-- The barrier sensors; neccessary for testbench purposes.
		front_barrier, under_barrier, after_barrier  : in  std_logic; 
		
		-- Output showing current barrier position (testing purposes)
		barrier_position	: out std_logic; 
		print_ticket		: out std_logic; -- Output to ticket printer
		-- Output to higher level which informs it that a car has entered
		car_passed			: out std_logic
		);
	end component;
	
	component exit_entity is
		port(
			clk         : in  std_logic; --clock 
		-- Ticket inserted to be validated
		ticketInserted		: in std_logic;
		-- Input from higher level to inform system of emergency state,
		-- where '0' indicates no emergency
		emergency			: in std_logic;
		-- Input from higher level stating if the ticket is valid
		validTicket    : in std_logic;
		-- The barrier sensors
		front_barrier, under_barrier, after_barrier  : in  std_logic;
		
		-- Output to the barrier to get it to raise or lower
		barrier_position     : out std_logic; 
		-- Output higher level to check if ticket is valid
		validate_ticket		: out std_logic; 
		-- Output to higher level which informs it that a car has entered
		car_passed			: out std_logic 
		);
	end component;
	
	begin	
		CREATE_ENTRANCES: for i in 0 to entry_bit_count-1 generate
							entryi : entry_entity port map(
									clk => clk,	
									button_pressed => entry_button_presses(i),
					-- This is a direct map from entry input to controller input
									emergency => emergency,
									carpark_status => cp_status,
									front_barrier => entry_front_sensors(i), 
									under_barrier => entry_under_sensors(i),
									after_barrier => entry_after_sensors(i),
									barrier_position => entry_barrier_pos(i),
									print_ticket => entry_print_tickets(i),
									car_passed => cars_entered(i)
									);
							end generate CREATE_ENTRANCES;
							
							
									
		CREATE_EXITS: for i in 0 to exit_bit_count-1 generate
							exiti : exit_entity port map(
									clk => clk, 
									ticketInserted => exit_inserted_tickets(i),
									emergency => emergency, 
									validTicket => exit_valid_tickets(i),
									front_barrier => exit_front_sensors(i), 
									under_barrier => exit_under_sensors(i),
									after_barrier => exit_after_sensors(i),
									barrier_position => exit_barrier_pos(i),
									validate_ticket => exit_validate_tickets(i),
									car_passed => cars_left(i)
									);
							end generate CREATE_EXITS;	
							
	alter_count:process(cars_entered, cars_left) is
				variable internal_count : unsigned(count'range):= (others => '0');
				begin
					if internal_count /= MAX_CAPACITY then
						for i in cars_entered'range loop
							if cars_entered(i) = '1' then
								internal_count := internal_count + 1;
							end if;
						end loop;
					else
						cp_status <= '1';
					end if;
								
					for i in cars_left'range loop
						if cars_left(i) = '1' then
							internal_count := internal_count - 1;
							cp_status <= '0';
						end if;
					end loop;
					count <= (MAX_CAPACITY - internal_count);
					entry_car_entered <= cars_entered;
					exit_car_entered <= cars_left;
				end process;
					
				status <= cp_status;			
end architecture controller_v1;
