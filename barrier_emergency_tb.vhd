library ieee;
use ieee.std_logic_1164.all;

entity barrier_emergency_tb is
end entity barrier_emergency_tb;

architecture v1 of barrier_emergency_tb is

    component barrier_entity
    port(
		clk         : in  std_logic; --clock 
        action_barrier      : in  std_logic; -- input from upper layer to signal an action should occur on the barrier
      	front_barrier, under_barrier, after_barrier  : in  std_logic; -- the barrier sensors
		
        effect_barrier     : out std_logic; --output to the barrier to get it to raise or lower
        at_barrier   : out std_logic; --output to system to determine if there is a car at front sensor
        passed_barrier   : out std_logic --output to system to say car is beyond barrier
    );
    end component barrier_entity;

    --------------------------------------
    -- Constants used within the design
    --------------------------------------
    constant T_clk      : time  	:= 10 ns;	-- define clock period
	constant T_CHANGE 	: time 		:= 11 ns;
    ------------------------------------------------------------------------------
    -- Stimulus signals - signals mapped to the input ports of the tested entity
    ------------------------------------------------------------------------------
    
    signal clk, action_barrier, front_barrier, under_barrier, after_barrier      : std_logic;
    
    -------------------------------------------------------------------------------
    -- Observed signals - signals mapped to the output ports of the tested entity
    -------------------------------------------------------------------------------
		
     signal effect_barrier, at_barrier, passed_barrier    : std_logic;

	begin
    -----------------------------------------------
    -- Unit Under Test instantiation and port map
    -----------------------------------------------
    UUT : barrier_entity
    port map (
        clk => clk,
        action_barrier => action_barrier,
        front_barrier     => front_barrier,
        under_barrier   => under_barrier,
        after_barrier   => after_barrier,
        effect_barrier => effect_barrier,
        at_barrier => at_barrier,
        passed_barrier => passed_barrier
    );
    ---------------------------------
    -- stimulus process (UUT inputs)
    ---------------------------------
    stimulus : process is
    begin
		-- Simulate an emergency happening in the case of an exit.
		-- No car at the barrier.  Predicted state : 0
		action_barrier <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '0' AND at_barrier = '0' AND passed_barrier = '0' report "Output1 Violation" severity failure;
		
		-- System responding to emergency decides to invoke action upon the barrier.  Predicted state change : 0 -> 2
		action_barrier <= '1'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '1' AND at_barrier = '1' AND passed_barrier = '0' report "Output2 Violation" severity failure;
		
		-- System remaining in emergency.  Predicted state : 2
		action_barrier <= '1'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '1' AND at_barrier = '1' AND passed_barrier = '0' report "Output3 Violation" severity failure;
		
		-- System no longer in emergency.  Predicted state change : 2 -> 0
		action_barrier <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '0' AND at_barrier = '0' AND passed_barrier = '0' report "Output4 Violation" severity failure;
	
		--Simulate an emergency happening in the case of an entrance.
		-- No car at the barrier.  Predicted state : 0
		action_barrier <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '0' AND at_barrier = '0' AND passed_barrier = '0' report "Output5 Violation" severity failure;
		
		-- Car appears at barrier.  Predicted state change : 0 -> 1
		action_barrier <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '0' AND at_barrier = '1' AND passed_barrier = '0' report "Output6 Violation" severity failure;
		
		-- Car waiting at barrier.  Predicted state : 1
		action_barrier <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '0' AND at_barrier = '1' AND passed_barrier = '0' report "Output7 Violation" severity failure;
		
		-- Car still waiting.  System responds with action, indicating spaces available.  Predicted state change : 1 -> 2
		action_barrier <= '1'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '1' AND at_barrier = '1' AND passed_barrier = '0' report "Output8 Violation" severity failure;
		
		-- Pretend barrier fully raised in this time period.  Car still detected at front of barrier.  Predicted state : 2
		action_barrier <= '1'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '1' AND at_barrier = '1' AND passed_barrier = '0' report "Output9 Violation" severity failure;
		
		-- The system responds to an emergency and sets the action to lower the barrier.  Predicted state change : 2 -> 0
		action_barrier <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '0' AND at_barrier = '0' AND passed_barrier = '0' report "Output10 Violation" severity failure;
		
		-- The system remains in emergency.  Go back to check car park status since car still there.  Predicted state change : 0 -> 1
		action_barrier <= '0'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '0' AND at_barrier = '1' AND passed_barrier = '0' report "Output11 Violation" severity failure;
		
		-- The system is out of emergency.  The entrance told to operate normally.  Predicted state change : 1 -> 2
		action_barrier <= '1'; front_barrier <= '1'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '1' AND at_barrier = '1' AND passed_barrier = '0' report "Output12 Violation" severity failure;
		
		-- The driver got annoyed and decided to shoot through.  Predicted state change : 2 -> 3
		action_barrier <= '1'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '1';
		wait for T_CHANGE;
		assert effect_barrier = '1' AND at_barrier = '0' AND passed_barrier = '1' report "Output13 Violation" severity failure;
		
		-- The system goes back to initial state.  Predicted state change : 3 -> 0
		action_barrier <= '0'; front_barrier <= '0'; under_barrier <= '0'; after_barrier <= '0';
		wait for T_CHANGE;
		assert effect_barrier = '0' AND at_barrier = '0' AND passed_barrier = '0' report "Output14 Violation" severity failure;
		
		wait;
    end process;

    ------------------------------
    -- clock generation process
    ------------------------------
    clk_generator : process is          
    begin
        while now <= 20*T_clk loop		-- define number of clock cycles to simulate
        
			clk <= '1';					-- create one full clock period every loop iteration
            wait for T_clk/2;
			clk <= '0';
            wait for T_clk/2;
			
        end loop;	   
		
		wait;		
        
    end process	clk_generator;

end architecture v1;

