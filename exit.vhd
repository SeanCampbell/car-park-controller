library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity exit_entity is
    port(
		clk         : in  std_logic; --clock 
		-- Ticket inserted to be validated
		ticketInserted		: in std_logic;
		-- Input from higher level to inform system of emergency state,
		-- where '0' indicates no emergency
		emergency			: in std_logic;
		-- Input from higher level stating if the ticket is valid
		validTicket    : in std_logic;
		-- The barrier sensors
		front_barrier, under_barrier, after_barrier  : in  std_logic;
		
		-- Output to the barrier to get it to raise or lower
		barrier_position     : out std_logic; 
		-- Output higher level to check if ticket is valid
		validate_ticket		: out std_logic; 
		-- Output to higher level which informs it that a car has entered
		car_passed			: out std_logic 
    );
end exit_entity;

architecture exit_v1 of exit_entity is
	
	type exit_state is (
		no_car, -- Initial state
		car_waiting,
		validatingTicket,
		barrier_open,
		car_through,
		state_of_emergency
	);
	
	signal current_state : exit_state; --Signal to track current state
	--Signals which take input from barrier component 
	signal car_at_barrier, car_passed_barrier : std_logic;
	--Signal which provides output to barrier component
	signal action_on_barrier : std_logic; 
	
	-- Declare the barrier component
	component barrier_entity is
		port(
			clk         : in  std_logic; --clock
			-- input from upper layer to signal an action should occur on the barrier
			action_barrier      : in  std_logic; 
			 -- the barrier sensors
			front_barrier, under_barrier, after_barrier  : in  std_logic;
			
			-- output to the barrier to get it to raise or lower
			effect_barrier		: out std_logic;
			-- output to system to determine if there is a car at front sensor
			at_barrier   		: out std_logic;
			 --output to system to say car is beyond barrier
			passed_barrier   	: out std_logic
		);
	end component;
	
	-- Declare which barrier architecture to use
	for barrier : barrier_entity use entity work.barrier_entity(barrier);
	
	begin
	--Do a direct port map for clarity, need to map all the component inputs
		barrier : barrier_entity
			port map( 	clk => clk,
						action_barrier => action_on_barrier,
						front_barrier => front_barrier,
						under_barrier => under_barrier,
						after_barrier => after_barrier,
						effect_barrier => barrier_position,
						at_barrier => car_at_barrier,
						passed_barrier => car_passed_barrier
						);
		
		-- This is the process which changes the current state
		update_state : process (clk, emergency)
			-- Locally store the next state in the sequence
			variable next_state : exit_state; 
			begin
				if emergency = '1' then
				-- If a car has passed the barrier in a state of emergency,
				 -- Go to CarThrough and output so the system above can decrement its count.
					if car_passed_barrier = '1' then 		
						current_state <= car_through;
					else	-- Otherwise, remain in a state of emergency.
						current_state <= state_of_emergency;
					end if;
					
				elsif rising_edge(clk) then
					case current_state is
						when no_car =>
						-- Check if there is a car at barrier
							if car_at_barrier = '1' then			
								next_state := car_waiting;
							else	-- Otherwise, do nothing.
								next_state := no_car;	
							end if;
							
						when car_waiting =>
						-- Make sure car is still there
							if car_at_barrier = '0' then 
								next_state := no_car;
						-- Check to see if ticket has been inserted,
							elsif ticketInserted = '1' then 
								next_state := validatingTicket;						
							else	-- Otherwise, do nothing.
								next_state := car_waiting;	
							end if;							
							
						when validatingTicket =>
						-- Make sure car is still there
							if car_at_barrier = '0' then 
								next_state := no_car;
							else	
						-- If ticket is removed, go back to waiting for a ticket
								if ticketInserted = '0' then 
									next_state := car_waiting;
						-- Otherwise, ticket present and if a valid ticket,
						--	allow exit.
								elsif validTicket = '1' then 
									next_state := barrier_open;
						-- Otherwise, wait to see if the system decides it is
						--  valid or is removed
								else	
									next_state := validatingTicket;
								end if;
							end if;

						when barrier_open =>
						-- Check if the car has passed the barrier.
							if car_passed_barrier = '1' then	
								next_state := car_through;
						-- Check if the car has backtracked.
							elsif car_at_barrier = '0' then		
								next_state := no_car;
							else
								next_state := barrier_open;
							end if;
							
						when car_through =>
						-- Always move straight onto next state
							next_state := no_car; 
							
						when state_of_emergency =>
						-- When recovering from emergency, check if a car
						-- has passed the barrier
						-- If so, notify the upper system
							if car_passed_barrier = '1' then	
								next_state := car_through;	
							else	-- Otherwise, jump to the no car state.
								next_state := no_car;
							end if;

						when others =>	-- Safety case; do nothing.
							null;
					end case;
					
					current_state <= next_state;         -- update current_state signal at end of process
				end if;
				
		end process;
	
	-- This is the process which deals with inputs
	-- Note of caution: Potentially confusing aspect lies ahead.
	-- Remember that the output action_on_barrier is linked 
	-- directly to the underlying barrier instance and is not the value of
	-- the output effect barrier of this state machine which is instead 
	-- simply the output of the underlying barrier; this output is simply
	-- used for testing to assert proper position handling.
	assign_outputs : process (current_state) is
		begin
			case current_state is
				when no_car =>
					action_on_barrier <= '0';
					validate_ticket <= '0';
					car_passed <= '0';

				when car_waiting =>	
					action_on_barrier <= '0';
					validate_ticket <= '0';
					car_passed <= '0';
					
				when validatingTicket =>
					action_on_barrier <= '0';
					validate_ticket <= '1';
					car_passed <= '0';
				
				when barrier_open =>
					action_on_barrier <= '1';
					validate_ticket <= '0';
					car_passed <= '0';
				
				when car_through =>
					action_on_barrier <= '1';
					validate_ticket <= '0';
					car_passed <= '1';
				
				when state_of_emergency =>
					action_on_barrier <= '1';
					validate_ticket <= '0';
					car_passed <= '0';
					
				when others =>	-- Safety case; do nothing.
					null;
			end case;
			
		end process;
		
end architecture exit_v1;

